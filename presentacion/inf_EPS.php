<?php
include 'presentacion/encabezado.php';
?>

<div class="container">
    <br>
    <h1 class="text-center">EPS</h1>
    <div class="text-center">
        <a href="https://www.mintrabajo.gov.co/lista-de-eps"> <h6>Si no esta la EPS que buscas, pulsa aqui para ver mas.</h6> </a>
    </div>
    <br>

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <!-- Famisanar-->
                <img class="card-img-top" src="presentacion/img/logoFamisanar.png" width="100" height="150"">
                <div class=" card-body">
                    <h3 class="card-title">Famisanar</h3>
                    <h6 class="card-title">Telefono: 01 8000 11 66 62</h6>
                    <h6 class="card-title">Bogotá: +57 601 307 8069</h6>
                    <h6 class="card-title">correo: servicioalcliente@famisanar.com.co</h6>
                    <a href="https://api.whatsapp.com/send/?phone=573006438831&text&type=phone_number&app_absent=0">
                        <h6 class="card-title">whatsapp 300 643 8831</h6>
                    </a>
                    <a href="https://www.famisanar.com.co/directorio-medico/">
                        <h6 class="card-title">Centros medicos</h6>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <!-- Compensar -->
                <img class="card-img-top" src="presentacion/img/logoCompensar.png" width="100" height="150"">
                <div class=" card-body">
                    <h3 class="card-title">Compensar</h3>
                    <h6 class="card-title">Telefono: 01 8000 91 5202</h6>
                    <h6 class="card-title">Bogotá: +57 601 4441234</h6>
                    <h6 class="card-title">sede Principal: Av. 68 #49A - 47, Bogotá D.C.</h6>
                    <a href="https://corporativo.compensar.com/salud/Compensar-EPS/Red-urgencias">
                        <h6 class="card-title">Centros medicos urgencias</h6>
                    </a>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <!-- Capital Salud-->
                <img class="card-img-top" src="presentacion/img/logoCapitalsalud.png" width="100" height="150"">
                <div class=" card-body">
                    <h3 class="card-title">Capital Salud</h3>
                    <h6 class="card-title">Telefono: 01 8000 12 2219</h6>
                    <h6 class="card-title">Bogotá: +57 601 7427257</h6>
                    <a href="https://portal.capitalsalud.gov.co/contactenos/">
                        <h6 class="card-title">Formulario institucional</h6>
                    </a>
                    <a href="https://www.capitalsalud.gov.co/puntos-de-atencion-bogota/">
                        <h6 class="card-title">Centros medicos</h6>
                    </a>
                    <br>
                </div>
            </div>
        </div>
        <!-- Salud total -->
        <div class="col-sm-6">
            <div class="card">
                <img class="card-img-top" src="presentacion/img/logoSaludtotal.png" width="100" height="150"">
                <div class=" card-body">
                    <h3 class="card-title">Salud Total</h3>
                    <h6 class="card-title">Telefono: 01 8000 114524</h6>
                    <h6 class="card-title">Bogotá: 601 307 8069</h6>
                    <a href="https://transaccional.saludtotal.com.co/chatbotMVC/ChatBot/Redireccionar?_ga=2.188499176.1944129413.1669313583-2061478868.1669225156">
                        <h6 class="card-title">Asesor en linea</h6>
                    </a>
                    <a href="https://transaccional.saludtotal.com.co/DIRECTORIOPOS/#">
                        <h6 class="card-title">Red de atencion y servicios</h6>
                    </a>
                    <a href="https://saludtotal.com.co/plan-de-beneficios-en-salud/numeros-de-atencion/">
                        <h6 class="card-title">Centros de atencion</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <!-- Nueva EPS-->
                <img class="card-img-top" src="presentacion/img/logoNuevaeps.png" width="100" height="150"">
                <div class=" card-body">
                    <h3 class="card-title">Nueva EPS</h3>
                    <h6 class="card-title">Telefono: 01 8000 95 2000</h6>
                    <h6 class="card-title">Bogotá: +57 601 307 7022</h6>
                    <a href="https://www.nuevaeps.com.co/urgencias-y-citas-m%C3%A9dicas">
                        <h6 class="card-title">Informacion Urgencias</h6>
                    </a>
                    <a href="https://www.nuevaeps.com.co/red-atencion#nogo/">
                        <h6 class="card-title">Centros medicos</h6>
                    </a>
                    <br>
                </div>
            </div>
        </div>
        <!-- Sura -->
        <div class="col-sm-6">
            <div class="card">
                <img class="card-img-top" src="presentacion/img/logoSura.png" width="100" height="150"">
                <div class=" card-body">
                    <h3 class="card-title">Sura</h3>
                    <h6 class="card-title">Telefono: 01 8000 519 519</h6>
                    <h6 class="card-title">Bogotá: 601 489 79 41</h6>
                    <a href="https://www.epssura.com/oficinas">
                        <h6 class="card-title">Oficinas de atencion</h6>
                    </a>
                    <a href="https://www.epssura.com/canales-de-contacto">
                        <h6 class="card-title">Formas de contacto</h6>
                    </a>
                    <a href="https://www.epssura.com/dir-medico">
                        <h6 class="card-title">Direccionamiento centros de atencion</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>