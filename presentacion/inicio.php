<?php
include 'presentacion/encabezado.php';
?>
<div class="container">
    <div class="row mt-3">
        <div class="col-4 align-center">
            <a href="https://www.facebook.com/BienestarUD/posts/pfbid0ziLuuMbeUpiyo2E8a1phUdrA7697ecymVYoYnDHvzVcvHAD1x8FHRjaHt7mVrsosl"><img src="presentacion/img/torneo.png" class="card-img-top" width="200" height="600"> </a>
        </div>
        <div class="col-6">
            <div class="text-center">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Sistema de informacion de apoyo a la salud sede tecnologica</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Futbolsala</h6>
                        <p class="card-text">El sistema de información planteado, busca apoyar y aligerar la carga del sistema de atención médica de Bienestar institucional UD, 
                            ofrecido durante la realización de los torneos de fútbol sala en la sede tecnológica, realizando aproximaciones a las lesiones que pueden 
                            presentar los estudiantes en base a los síntomas que tengan, ya que, el fútbol sala al ser un deporte de contacto hay la posibilidad de que puedan haber algunos accidentes, 
                            y faltas, en estas situaciones los estudiantes pueden resultar con heridas leves, las cuales pueden tratarse con el correcto cuidado y procedimiento, en el cual el sistema de información entra en escena, 
                            ofreciendo recomendaciones y consejos para poder tratarse de manera rápida a su vez efectiva, y en caso de que sea una situación que requiera de la valoración de un profesional de la salud 
                            poder solicitar un turno para su atención con el doctor de la sede tecnológica de bienestar institucional, o de ser necesario, aconsejarle al estudiante herido, que se remita con la EPS a la cual esté afiliado,  
                            ofreciéndole información de contacto de algunas de las EPS más comunes.</p>
                        <a href="https://bienestar.udistrital.edu.co/servicios/deportes" class="card-link">Link Bienestar Institucional</a>
                        <a href="http://www1.udistrital.edu.co:8080/es/web/facultad-tecnologica" class="card-link">Link Facultad tecnologica</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>