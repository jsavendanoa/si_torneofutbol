<?php
include 'presentacion/encabezado.php';
?>
<br>
<h1 class="text-center">Personal de salud</h1>

<br>
<div class="container">
    <div class="row mt-3">
        <div class="col-3 align-center">
            <img src="presentacion/img/doctora.jpg" class="card-img-top" width="200" height="500">
        </div>
        <div class="col-6">
            <div class="text-center">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Mañana / Tarde</h5>
                        <h3 class="card-subtitle mb-2 text-muted">Mañana</h3>
                        <h5 class="card-title">Horario 7:00am - 2:00pm</h5>
                        <h6 class="card-title">1 Medico encargado (Dr. Laureno Pieteit) y 2 aux. enfermeria</h6>
                        <h6 class="card-title">Forma de contacto atravez de Bienestar Institucional UD, o en la oficina ubicada en el bloque 13 primer piso</h6>
                        <br>
                        <h3 class="card-subtitle mb-2 text-muted">Tarde</h3>
                        <h5 class="card-title">Horario 2:00pm - 8:30pm</h5>
                        <h6 class="card-title">1 Medico encargado y 2 aux. enfermeria</h6>
                        <h6 class="card-title">Forma de contacto atravez de Bienestar Institucional UD, o en la oficina ubicada en el bloque 13 primer piso</h6>
                        <br>
                        <h6>Un total de 2 medicos y 4 auxiliares de enfermeria en turnos rotativos.</h6>
                        <a href="https://bienestar.udistrital.edu.co/contacto" class="card-link">Contacta Bienestar Institucional</a>
                        <a href="https://bienestar.udistrital.edu.co/servicios/desarrollo-humano-y-salud" class="card-link">Desarrollo humano y salud</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-3 align-center">
            <img src="presentacion/img/doctor.png" class="card-img-top" width="200" height="500">
        </div>
    </div>
</div>