<?php
include 'presentacion/encabezado.php';
?>
<br>
<h1 class="text-center">Bienestar Institucional UDFJC</h1>

<br>
<div class="container">
    <div class="row mt-3">
        <div class="col-4 align-center">
            <a href="https://bienestar.udistrital.edu.co/contacto"><img src="presentacion/img/contactenosBienestarUD.png" class="card-img-top" width="200" height="600"> </a>
        </div>
        <div class="col-6">
            <div class="text-center">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Telefono de contacto: 3239300</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Extension sede tecnologica 5017 </h6>
                        <p class="card-text">El Centro de Bienestar Institucional brinda servicios de salud de primer nivel, 
                            donde se conformó una IPS con objeto social diferente entre los cuáles se destacan los de atención en medicina, odontología y enfermería. 
                            De igual forma realiza acompañamiento psicológico y brinda el servicio de fisioterapia. Estos servicios se ofrecen en las cinco facultades de la Universidad, 
                            facilitando el acceso a toda la comunidad universitaria. Además de generar estrategias entorno a programas de prevención de embarazo no deseado, 
                            prevención de consumo de sustancias psicoactivas, y hábitos saludables.</p>
                        <br>
                        <p class="card-text">El servicio de medicina que brinda la Universidad Distrital Francisco José de Caldas a través del Centro de Bienestar Institucional 
                            es la atención básica de primer nivel, el acompañamiento a un nivel superior de complejidad o los servicios de interconsulta del Centro de Bienestar Institucional.
                            <br>
                            Asi mismo la universidad Distrital Francisco Jose de Caldas, tambien ofrece un servicio de fisioterapia, que tiene como objetivo rehabilitar las patologías osteoartromusculares, tratar el dolor de articulaciones y esclerosis, 
                            brindando una atención eficiente y eficaz a los diferentes estamentos de la comunidad Universitaria U.D. Este servicio se presta con la remisión médica por parte del área de salud.
                            <br>
                            Ten en cuenta que cualquier persona que haga parte de la comunidad universitaria: estudiantes, docentes, funcionarios, contratistas, trabajadores y demás puede hacer uso de estos servicios.
                        </p>
                        <a href="https://bienestar.udistrital.edu.co/contacto" class="card-link">Contacta Bienestar Institucional</a>
                        <a href="https://bienestar.udistrital.edu.co/servicios/desarrollo-humano-y-salud" class="card-link">Desarrollo humano y salud</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>