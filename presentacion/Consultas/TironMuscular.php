<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Tiron muscular</dd>

  <dt class="col-sm-3">Recomendaciones:</dt>
  <dd class="col-sm-9">
    <p>Lo mas antes posible estirar la extremidad sin flexionarla por un momento</p>
    <p>Realizar leves masajes sobre el tumulto</p>
    <p>Relajar el musculo, no tensionarlo</p>
    <p>Poner bolsa con hielo en la extremidad</p>
    <p>Flexionar y realizar acciones leves de elongacion o estiramiento conforme la rotacion
        o movimiento normal de la extremidad.</p>
    <p>Controlar la respiracion</p>
  </dd>

  <dt class="col-sm-3 text-truncate">Sintomas:</dt>
  <dd class="col-sm-9">
    <p>Recogimiento del tendón en la zona afectada, como tumulto o bola, no ruptura</p>
    <p>Impedimento a la hora de realizar ciertos movimientos en la zona del dolor</p>
    <p>Impide la extensión muscular completa (movimientos limitados)</p>
    <p>Parálisis en la zona (momentánea o temporal)</p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">Puede darse por un mal estiramiento/calentamiento antes de realizar el deporte, o por malos movimientos</dd>

  <dt class="col-sm-3">Tiempo de recuperacion</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-3">30 minutos</dt>
      <dd class="col-sm-9">De 5 a 15 minutos se podra sentir mejoria, pero habra algo de dolor</dd>
    </dl>
  </dd>
</dl>