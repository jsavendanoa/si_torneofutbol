<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Fractura o Dislocacion del hueso</dd>

  <dt class="col-sm-3">Recomendaciones:</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-12">Se recomienda dirigirse a la EPS lo mas antes posible</dt>
      <p>Evitar movimientos bruscos en el traslado</p>
      <p>No mover la extremidad en el translado</p>
    </dl>
  </dd>

  <dt class="col-sm-3 text-truncate">Sintomas:</dt>
  <dd class="col-sm-9">
    <p> Rotura o desplazamiento de un hueso de su ubicación a otro punto 
      (el hueso no esta en la posición que debería) </p>
    <p> Si la ubicacion del hueso no es la correta (dislocacion), posiblemente se puede hacer movimiento, pero no es muy recomendable </p>
    <p> Si no puede mover la extremidad a voluntad, o se mueve de maneras que no deberia es ruptura. </p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">La extremidad se mueve libremente, realizando movimientos que no deberia</dd>

</dl>