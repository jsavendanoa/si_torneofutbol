<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Raspon o herida leve</dd>

  <dt class="col-sm-3">recomendacion</dt>
  <dd class="col-sm-9">
    <p>Trata de usar isodine o alcohol en la herida para desinfectarla</p>
    <p>Cubrela con gasa o en su defecto una cura, 
      pero recuerda cambiarla con frecuencia y mantener limpia la zona</p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">Al ser leve y/o superficial, solo basta cuidar lo suficiente, ya que puede infectarse y complicarse</dd>
  </dd>
</dl>