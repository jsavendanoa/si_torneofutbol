<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Fatiga muscular</dd>

  <dt class="col-sm-3">Recomendaciones:</dt>
  <dd class="col-sm-9">
    <p> Relajacion al musculo o extremidad </p>
    <p>Autocuidado y conciencia con el sobre-uso</p>
    <p>Cremas en base a caléndula</p>
    <p>Estiramiento del musculo suave</p>
    <p>Post partido, pañitos de agua tibia, estiramiento, y descanso del musculo</p>
    <p>Post partido, no realizar actividad física intensa, y realizar cardio, actividad física a un ritmo muy suave y controlado</p>
    <p>Alternar descanso con actividad física suave</p>
  </dd>

  <dt class="col-sm-3 text-truncate">Sintomas:</dt>
  <dd class="col-sm-9">
    <p> Los musculos de las extremidades resultan tensos al realizar cada accion </p>
    <p> Puede generar cansancio excesivo </p>
    <p> Debido al cansancio excesivo puede generar recogimiento de tendones </p>
    <p> Al momento de tocar la zona, estara tensionada involuntariamente </p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">Sobre-exigir el musculo antes o durante la realizacion del deporte puede causar fatiga muscular.</dd>

  <dt class="col-sm-3">Tiempo de recuperacion</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-3">1 a 5 dias</dt>
      <dd class="col-sm-9">Dependiendo el estado del musculo, y la rutina puede que requiera menos tiempo.</dd>
    </dl>
  </dd>
</dl>