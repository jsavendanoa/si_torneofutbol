<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Esguince de tobillo - Grado 2</dd>

  <dt class="col-sm-3">Recomendaciones:</dt>
  <dd class="col-sm-9">
    <p>Baño con agua fria</p>
    <p>Paños con agua fria sobre la zona</p>
    <p>Crema a base de calendula o tambien conocida como (crema verde o caliente).</p>
    <p>Vendaje todo el tiempo en la cotidianidad de 2 a 3 semanas</p>
    <p>Masajes intermitentes</p>
    <p>Ejercicios de rotacion de pie sin sobre esfuerzo</p>
  </dd>

  <dt class="col-sm-3 text-truncate">Sintomas:</dt>
  <dd class="col-sm-9">
    <p>Dolor en el tobillo</p>
    <p>Puede apoyar el pie, pero solo con la punta de los dedos, (no apoya por completo el pie)</p>
    <p>Mayor dificultad al caminar</p>
    <p>Mover el pie duele</p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">Muy similar al esguince grado 1, pero la diferencia es que los movimientos seran mas limitados, 
    y con un dolor mas intenso, por lo que no dejara apoyar el pie por completo en el suelo, se recomienda por aproximadamente 2 semanas
   tener cuidado en el dia a dia, ya que puede empeorar con una mala fuerza.</dd>

  <dt class="col-sm-3">Tiempo de recuperacion</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-4">2 a 3 semanas</dt>
      <dd class="col-sm-8">Antes de este tiempo se recomienda no esforzar demasiado el pie</dd>
    </dl>
  </dd>
</dl>