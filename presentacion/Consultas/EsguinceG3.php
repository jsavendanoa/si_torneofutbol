<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Esguince de tobillo - Grado 3</dd>

  <dt class="col-sm-3">Recomendaciones:</dt>
  <dd class="col-sm-9">
    <p>No mover en lo posible el tobillo</p>
    <p>Tener cuidado en el desplazamiento a la EPS</p>
    <dl class="row">
      <dt class="col-sm-12">Dirigirse a la EPS lo mas antes posible</dt>
    </dl>
  </dd>

  <dt class="col-sm-3 text-truncate">Sintomas:</dt>
  <dd class="col-sm-9">
    <p>Dolor en el tobillo intenso</p>
    <p>No puede apoyar el pie</p>
    <p>No puede mover el pie</p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">Lo mas recomendable es dirigirse a la EPS con precaucion, y asi no empeorar la situacion.</dd>

  <dt class="col-sm-3">Tiempo de recuperacion</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-4">2 a 4 semanas</dt>
      <dd class="col-sm-8">Antes de este tiempo se recomienda no esforzar demasiado el pie</dd>
    </dl>
  </dd>
</dl>