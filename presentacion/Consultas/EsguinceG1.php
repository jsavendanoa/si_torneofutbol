<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Esguince de tobillo - Grado 1</dd>

  <dt class="col-sm-3">Recomendaciones:</dt>
  <dd class="col-sm-9">
    <p>Baño con agua fria</p>
    <p>Paños con agua fria sobre la zona</p>
    <p>Crema a base de calendula o tambien conocida como (crema verde o caliente).</p>
    <p>Vendaje todo el tiempo en la cotidianidad de 2 a 3 semanas</p>
    <p>Masajes intermitentes</p>
    <p>Ejercicios de rotacion de pie sin sobre esfuerzo</p>
  </dd>

  <dt class="col-sm-3 text-truncate">Sintomas:</dt>
  <dd class="col-sm-9">
    <p>Dolor en el tobillo</p>
    <p>Se puede apoyar la plante del pie sobre una superficie con un poco de dificultad</p>
    <p>Dificultad al caminar</p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">Es posible caminar, y mover el pie con cierta molestia, si no puedes mover el pie o apoyarlo, 
    puede ser esguince grado 2 o 3.</dd>

  <dt class="col-sm-3">Tiempo de recuperacion</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-4">2 a 3 semanas</dt>
      <dd class="col-sm-8">Antes de este tiempo se recomienda no esforzar demasiado el pie</dd>
    </dl>
  </dd>
</dl>