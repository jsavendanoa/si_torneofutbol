<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Ruptura muscular</dd>

  <dt class="col-sm-3">Recomendaciones:</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-12">Se recomienda dirigirse a la EPS lo mas antes posible</dt>
      <p>Evitar movimientos bruscos en el traslado</p>
    </dl>
  </dd>

  <dt class="col-sm-3 text-truncate">Sintomas:</dt>
  <dd class="col-sm-9">
    <p> No puede mover la extremidad </p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">La extremidad se mueve libremente conforme la rotacion normal que deberia tener</dd>

</dl>