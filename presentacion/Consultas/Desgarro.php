<dl class="row">
  <dt class="col-sm-3">Herida o lesion</dt>
  <dd class="col-sm-9">Desgarro Muscular</dd>

  <dt class="col-sm-3">Recomendaciones:</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-12">Se recomienda dirigirse y solicitar valoracion del personal medico de la facultad</dt>
    </dl>
  </dd>

  <dt class="col-sm-3 text-truncate">Sintomas:</dt>
  <dd class="col-sm-9">
    <p> Dolor mas intenso que un recogimiento de tendon </p>
    <p> No hay tumulto o "bola" visible </p>
    <p> Para realizar desplazamiento o movimiento de la extremidad se dificulta </p>
  </dd>

  <dt class="col-sm-3">Tener en cuenta:</dt>
  <dd class="col-sm-9">Se genera al realizar acciones o movimientos mas alla de los limites a los que estan acostumbrados los musculos.</dd>

  <dt class="col-sm-3">Tiempo de recuperacion</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-3">1 a 2 semanas</dt>
      <dd class="col-sm-9">Si después de 30 a 35 min de un tirón muscular o calambre sigue el dolor intenso es desgarro muscular</dd>
    </dl>
  </dd>
</dl>