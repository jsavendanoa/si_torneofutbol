<?php
include 'presentacion/encabezado.php';
?>
<div class="container">
    <!-- Container -->
    <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/ResultadoTest.php") ?>">
        <br>
        <div class="row">
            Ingresa tu nombre
            <input maxlength="48" onkeypress="return (event.charCode<=90 && event.charCode>=65 || event.charCode<=122 && event.charCode>=97 || event.charCode==32)" 
             type="text" name="name">
            <!-- OneKeyPress ayuda a que el usuario solo pueda ingresar los caracteres que queremos
            esto por medio del codigo de cada caracter por ASCII -->
        </div>
        <br>
        <!-- pregunta 0 -->
        <div class="row">
            <div class="col-12">
                <h5>¿Te duele algo o tienes alguna herida o golpe?</h5>
            </div>
            <div class="col-2">
                Si
                <input class="col-6" type="radio" name="pregunta0" value="SI">
            </div>
            <div class="col-2">
                No
                <input class="col-6" type="radio" name="pregunta0" value="NO" checked>
            </div>
        </div>
        <br>

        <!-- pregunta 1 -->
        <div class="row">
            <div class="col-12">
                <h5>¿Puedes ver el hueso, o la extremidad se mueve involuntariamente de una forma anormal?</h5>
            </div>
            <div class="col-2">
                Si
                <input class="col-6" type="radio" name="pregunta1" value="SI">
            </div>
            <div class="col-2">
                No
                <input class="col-6" type="radio" name="pregunta1" value="NO" checked>
            </div>
        </div>
        <br>

        <!-- pregunta 2-->
        <div class="row">
            <div class="col-12">
                <h5>¿Te duele el pie, hay incomodidad o no puedes apoyar alguno de los pies en el suelo?</h5>
            </div>
            <div class="col-3">
                No puedes apoyar el pie y hay dolor
                <input class="col-6" type="radio" name="pregunta2" value="3">
            </div>
            <div class="col-3">
                Apoyas solo la punta de los dedos
                <input class="col-6" type="radio" name="pregunta2" value="2">
            </div>
            <div class="col-3">
                Apoyas la planta del pie con dificultad
                <input class="col-6" type="radio" name="pregunta2" value="1">
            </div>
            <div class="col-3">
                No hay dolor en el pie
                <input class="col-6" type="radio" name="pregunta2" value="NO" checked>
            </div>
        </div>
        <br>

        <!-- pregunta 3-->
        <div class="row">
            <div class="col-12">
                <h5>¿Puedes mover la extremidad?</h5>
            </div>
            <div class="col-3">
                NO
                <input class="col-6" type="radio" name="pregunta3" value="ruptura">
            </div>
            <div class="col-3">
                Si, pero con mucha dificultad
                <input class="col-6" type="radio" name="pregunta3" value="desgarro">
            </div>
            <div class="col-3">
                Si, con dolor y veo tumultos
                <input class="col-6" type="radio" name="pregunta3" value="Tiron">
            </div>
            <div class="col-3">
                Si, sin dolor ni anormalidades
                <input class="col-6" type="radio" name="pregunta3" value="SI" checked>
            </div>
        </div>
        <br>

        <!-- pregunta 4-->
        <div class="row">
            <div class="col-12">
                <h5>¿los musculos de las extremidades estan tensos, y con cansancio excesivo?</h5>
            </div>
            <div class="col-2">
                Si
                <input class="col-6" type="radio" name="pregunta4" value="SI">
            </div>
            <div class="col-2">
                No
                <input class="col-6" type="radio" name="pregunta4" value="NO" checked>
            </div>
        </div>
        <br>

        <!-- pregunta 5-->
        <div class="row">
            <div class="col-12">
                <h5>¿Tienes un dolor leve y/o un raspon?</h5>
            </div>
            <div class="col-2">
                Si
                <input class="col-6" type="radio" name="pregunta5" value="SI">
            </div>
            <div class="col-2">
                No
                <input class="col-6" type="radio" name="pregunta5" value="NO" checked>
            </div>
        </div>
        <br>

        

        <!-- Boton enviar -->
        <button type="submit" class="btn btn-info" name="Enviar">Enviar formulario</button>

    </form>
    <!-- Fin container -->
</div>