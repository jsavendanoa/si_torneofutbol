<?php
include 'presentacion/encabezado.php';
?>
<div class="container">
    <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/consulta.php") ?>">
        <!-- Container -->
        <br>
        <h2 class="text-center">Por favor selecciona la consulta que deseas realizar</h2>
        <h6>Si estas seguro de lo que requieres, realiza la consulta. <br>
            Pero, si no sabes que lesion o herida tienes en concreto, como tratarla o cual seria la mejor forma de afrontarla,
            te recomendamos que vayas a la pestaña de test de valoracion, donde podras saber mejor que camino tomar.
        </h6>
        <br>
        <select name="seleccion" class="form-select" aria-label="Default select example">
            <option selected>Selecciona la acción requerida</option>
            <option value="1">Buscar informacion EPS</option>
            <option value="2">Buscar informacion Bienestar UD</option>
            <option value="3">Buscar horarios atencion Personal de la salud</option>
            <option value="12">Raspon o herida superficial muy leve</option>
            <option value="4">Esguince de tobillo - grado 1</option>
            <option value="5">Esguince de tobillo - grado 2</option>
            <option value="6">Esguince de tobillo - grado 3</option>
            <option value="7">Tiron muscular o calambre</option>
            <option value="8">Desgarro muscular</option>
            <option value="9">Ruptura muscular</option>
            <option value="10">Fractura o dislocacion de hueso</option>
            <option value="11">Fatiga muscular</option>
        </select>
        <br>
        <button type="submit" class="btn btn-info" name="consultar">buscar</button>
    </form>
    <!-- Fin container -->
</div>

<br>
<div class="container">
    <div class="row">
        <?php
        //Crear consulta
        if (isset($_POST["consultar"])) {

            switch ($_REQUEST['seleccion']) {
                default:
                    echo "<h3>Debes elejir una opcion del cuadro de seleccion :)</h3>";
                    break;
                case "1":
                    echo "<h3> Para ver la informacion relacionada con las EPS, 
                por favor dirigete a la pestaña (informacion EPS) </h3>";
                    break;

                case "2":
                    echo "<h3> Para ver la informacion relacionada con Bienestar institucional, 
                por favor dirigete a la pestaña (informacion Bienestar UD) </h3>";
                    break;

                case "3":
                    echo "<h3> Para ver la informacion relacionada con el personal de salud, incluyendo los horarios, 
                    por favor dirigete a la pestaña (informacion Personal salud) </h3>";
                    break;

                case "4":
                    include 'presentacion/Consultas/EsguinceG1.php';
                    break;

                case "5":
                    include 'presentacion/Consultas/EsguinceG2.php';
                    break;

                case "6":
                    include 'presentacion/Consultas/EsguinceG3.php';
                    break;

                case "7":
                    include 'presentacion/Consultas/TironMuscular.php';
                    break;

                case "8":
                    include 'presentacion/Consultas/Desgarro.php';
                    break;

                case "9":
                    include 'presentacion/Consultas/RupturaMuscular.php';
                    break;

                case "10":
                    include 'presentacion/Consultas/fractura.php';
                    break;

                case "11":
                    include 'presentacion/Consultas/Fatiga.php';
                    break;

                case "12":
                    include 'presentacion/consultas/raspon.php';
                    break;
            }
        }
        ?>
    </div>
</div>