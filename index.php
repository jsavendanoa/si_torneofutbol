<?php
$pid = "";
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}
?>

<!DOCTYPE html>
<html>
    <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" >
    </head>

    <?php
    if($pid != ""){
        include $pid;
    } else{
        include 'presentacion/inicio.php';
    }
    ?>
</html>